/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  NavigatorIOS,
  StyleSheet,
  Text,
  View
} from 'react-native';

import TaskList from './app/components/TaskList';

export default class SimpleToDo extends Component {
  render() {
    return (
      <NavigatorIOS
        initialRoute={{
          component: TaskList,
          title: 'Tasks'
        }}
        style={ styles.container } />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

AppRegistry.registerComponent('SimpleToDo', () => SimpleToDo);
