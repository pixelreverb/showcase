import { Platform, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    editTaskContainer: {
        flex: 1,
        paddingTop: Platform.OS === 'ios'? 64 : 54
    },
    editTask: {
        fontSize: 36
    }
});

export default styles;