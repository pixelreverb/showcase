import React from 'react';
import {
    DatePickerIOS, Text, View
} from 'react-native';

import styles from './styles';

import ExpandableRow from '../ExpandableRow';

export default class EditTask extends React.Component {

    constructor (props) {
        super(props);

        this.state = {
            date: new Date()
        };
    }

    _onDateChange (date) {
        this.setState({
            date
        })
    }

    render () {

        return (
            <View style={ styles.editTaskContainer }>
                <ExpandableRow 
                    title={ 'Due on' } >
                    <DatePickerIOS
                        date={ this.state.date }
                        onDateChange={ (date) => this._onDateChange(date) }
                        />
                    </ExpandableRow>
                <Text style={ styles.editTask }>
                </Text>
            </View>
        );
    }
}