import React, { PropTypes } from 'react';
import { 
    Text, TouchableHighlight, View
} from 'react-native';

import styles from './styles';

export default class TaskListRow extends React.Component {

    static propTypes = {
        completed:  PropTypes.bool.isRequired,
        id:         PropTypes.string.isRequired,
        onLongPress: PropTypes.func.isRequired,
        onPress:    PropTypes.func.isRequired,
        text:       PropTypes.string.isRequired
    }

    constructor (props) {
        super(props);
    }

    render () {

        const isCompleted = this.props.completed ? 'line-through' : 'none';
        const textStyle = {
            fontSize: 20,
            textDecorationLine: isCompleted
        }

        return (
            <View style={{ 
                padding: 10, 
                backgroundColor: this.props.completed ? '#d5dbde' : '#e6e6e6' 
            }}>
                <TouchableHighlight
                    onLongPress={ () => this.props.onLongPress(this.props.id) }
                    onPress={ () => this.props.onPress(this.props.id) }
                    underlayColor={ '#d5dbde' }>
                    <Text style={ textStyle }>
                        { this.props.text }
                    </Text>
                </TouchableHighlight>
            </View>

        );
    }
}