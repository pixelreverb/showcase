import React, { PropTypes } from 'react';
import {
    LayoutAnimation, Text, TouchableHighlight, View
} from 'react-native';

import styles from './styles';

export default class ExpandableRow extends React.Component {

    static propTypes = {
        title: PropTypes.string.isRequired
    }

    constructor (props) {
        super(props);

        this.state = {
            expanded: false
        };
    }

    // componentWillUpdate () {
    //     LayoutAnimation.linear();
    // }

    _expandRow () {

        LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);

        this.setState({
            expanded: !this.state.expanded
        });
    }

    render () {
        return (
            <View style={ styles.expandableRowContainer }>
                <View>
                    <TouchableHighlight
                        onPress={ () => this._expandRow() }
                        underlayColor={ '#d3d3d3' } >
                        <Text style={ styles.visibleContent }>
                            {this.props.title}
                        </Text>
                    </TouchableHighlight>
                </View>
                <View 
                    style={[
                        styles.hiddenContent, 
                        this.state.expanded ? {} : { maxHeight: 0 } 
                    ]} >
                        {this.props.children}
                </View>
            </View>
        );
    }
}