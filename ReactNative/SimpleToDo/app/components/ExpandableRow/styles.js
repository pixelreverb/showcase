import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    expandableRowContainer: {
        flex: 1,
        padding: 10,
        paddingTop: 0
    },
    hiddenContent: {
        overflow: 'hidden'
    },
    visibleContent: {
        fontSize: 20
    }
});

export default styles;