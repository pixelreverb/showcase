import React from 'react';
import {
    AsyncStorage, ListView, Text, TextInput, View
} from 'react-native';

import styles from './styles';

import TaskListRow from '../TaskListRow';
import EditTask from '../EditTask';

export default class TaskList extends React.Component {
    
    constructor (props) {
        super(props);

        const dataSource = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            }),
            listOfTasks: [],
            text: ''
        };
    }

    componentDidMount () {
        //AsyncStorage.clear();
        this._updateList();
    }

    async _getListOfTasks() {
        let response = await AsyncStorage.getItem('listOfTasks');
        let listOfTasks = await JSON.parse(response) || [];

        return listOfTasks;
    }

    async _updateList () {
        let listOfTasks = await this._getListOfTasks();

        this.setState({ listOfTasks });

        this._changeTextInputValue('');
    }

    async _addTask () {

        const task = {
            completed: false,
            text: this.state.text
        }

        const listOfTasks = [...this.state.listOfTasks, task];

        await AsyncStorage.setItem('listOfTasks', JSON.stringify(listOfTasks));

        this._updateList();
    }

    async _editTask (rowId) {
        let listOfTasks = await this._getListOfTasks();

        this.props.navigator.push({
            component: EditTask,
            title: listOfTasks[rowId].text
        });
    }

    _changeTextInputValue (text) {
        this.setState({ text });
    }

    async _completeTask (rowId) {
        let listOfTasks = await this._getListOfTasks();

        listOfTasks[rowId].completed = true;
         
        await AsyncStorage.setItem('listOfTasks', JSON.stringify(listOfTasks));

        this.setState({ listOfTasks });
    }

    _renderRow (data, rowId) {
        return (
            <TaskListRow
                completed={ data.completed }
                id={ rowId }
                onLongPress={ (rowId) => this._editTask(rowId) }
                onPress={ (rowId) => this._completeTask(rowId) }
                text={ data.text } />
        );
    }

    render() {

        const ds = this.state.dataSource.cloneWithRows(this.state.listOfTasks); 

        return (
            <View style={ styles.container }>
                <View style={ styles.textInputContainer }>
                    <TextInput
                        autoCorrect={false}
                        placeholder="New Task"
                        onChangeText={ (text) => this._changeTextInputValue(text) }
                        onSubmitEditing={ () => this._addTask() }
                        returnKeyType={ 'done' }
                        style={ styles.textInput }
                        value={ this.state.text } />
                </View>
                <ListView
                    style={ styles.listView }
                    dataSource={ ds }
                    enableEmptySections={ true }
                    automaticallyAdjustContentInsets={ false }
                    renderRow= { (data, sectionId, rowId) => this._renderRow(data, rowId) } />
            </View>
        );
    }
}