import { Platform, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  textInputContainer: {
    padding: 10,
    paddingTop: Platform.OS === 'ios'? (64 + 10) : (54 + 10),
    backgroundColor: '#848'
  },
  textInput: {
    height: 40, 
    color: 'white',
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: '#f8f',
    borderRadius: 5,
    padding: 5
  },
  listView: {
    backgroundColor: '#e6e6e6'
  }
});

export default styles;