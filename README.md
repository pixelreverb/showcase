# Showcase repository #

This repository is intended to hold some code examples and sample projects.
Projects and code samples may vary in size and complexity.

### Content ###

* Android
    * Compass application
    
* React Native
    * Simple Todo Application

* Symfony
    * Push server project
    * Staff administration project