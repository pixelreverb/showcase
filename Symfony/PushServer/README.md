PushServer
==========

A simple push notification server project.

- Secured by a token set to specific lifetime
- Devices can be registered with a valid token from APN/GCM
- Creates and saves notifications
- Emits notifications directly after creation
- Allows to manually (retry) sending notifications
