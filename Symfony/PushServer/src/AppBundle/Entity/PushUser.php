<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 20.06.17
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @SWG\Definition(type="object")
 *
 * @ORM\Entity
 * @ORM\Table(name="push_users")
 */
class PushUser
{
    /**
     * @SWG\Property(format="int32")
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $appId;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $uuid;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $deviceToken;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $deviceType;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $deviceModel;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $deviceVersion;

    /**
     * @SWG\Property
     * @var datetime
     *
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @SWG\Property
     * @var datetime
     *
     * @ORM\Column(type="datetime")
     */
    private $modified;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="PushNotification", mappedBy="pushUser")
     */
    private $pushNotifications;

    /**
     * PushUser constructor.
     */
    public function __construct()
    {
        $this->pushNotifications = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * @param mixed $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getDeviceToken()
    {
        return $this->deviceToken;
    }

    /**
     * @param mixed $deviceToken
     */
    public function setDeviceToken($deviceToken)
    {
        $this->deviceToken = $deviceToken;
    }

    /**
     * @return mixed
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * @param mixed $deviceType
     */
    public function setDeviceType($deviceType)
    {
        $this->deviceType = $deviceType;
    }

    /**
     * @return mixed
     */
    public function getDeviceModel()
    {
        return $this->deviceModel;
    }

    /**
     * @param mixed $deviceModel
     */
    public function setDeviceModel($deviceModel)
    {
        $this->deviceModel = $deviceModel;
    }

    /**
     * @return mixed
     */
    public function getDeviceVersion()
    {
        return $this->deviceVersion;
    }

    /**
     * @param mixed $deviceVersion
     */
    public function setDeviceVersion($deviceVersion)
    {
        $this->deviceVersion = $deviceVersion;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getPushNotifications()
    {
        return $this->pushNotifications;
    }

    /**
     * @param mixed $pushNotifications
     */
    public function setPushNotifications($pushNotifications)
    {
        $this->pushNotifications = $pushNotifications;
    }
}