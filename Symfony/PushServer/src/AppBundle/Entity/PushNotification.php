<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 20.06.17
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @SWG\Definition(type="object")
 *
 * @ORM\Entity
 * @ORM\Table(name="push_notifications")
 */
class PushNotification
{
    /**
     * @SWG\Property(format="int32")
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $icon;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $sound;

    /**
     * @SWG\Property
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $data;

    /**
     * @SWG\Property
     * @var datetime
     *
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @SWG\Property
     * @var datetime
     *
     * @ORM\Column(type="datetime")
     */
    private $modified;

    /**
     * @SWG\Property
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isSent;

    /**
     * @ORM\ManyToOne(targetEntity="PushUser", inversedBy="pushNotifications")
     */
    private $pushUser;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return mixed
     */
    public function getSound()
    {
        return $this->sound;
    }

    /**
     * @param mixed $sound
     */
    public function setSound($sound)
    {
        $this->sound = $sound;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getIsSent()
    {
        return $this->isSent;
    }

    /**
     * @param mixed $isSent
     */
    public function setIsSent($isSent)
    {
        $this->isSent = $isSent;
    }

    /**
     * @return mixed
     */
    public function getPushUser()
    {
        return $this->pushUser;
    }

    /**
     * @param mixed $pushUser
     */
    public function setPushUser($pushUser)
    {
        $this->pushUser = $pushUser;
    }
}