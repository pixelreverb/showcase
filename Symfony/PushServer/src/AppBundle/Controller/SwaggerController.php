<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 20.06.17
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class SwaggerController extends Controller
{

    /**
     * @Route("apidoc", name="api_doc")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function swaggerAction() {
        return $this->render('swagger/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }
}