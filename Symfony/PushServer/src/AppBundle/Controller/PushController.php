<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 20.06.17
 */

namespace AppBundle\Controller;


use AppBundle\Entity\PushNotification;
use AppBundle\Entity\PushUser;
use Doctrine\ORM\EntityManager;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Swagger\Annotations as SWG;

class PushController extends Controller
{
    private $requiredUserFields = [
        "appId", "uuid", "deviceToken", "deviceType"
    ];

    private $requiredNotificationFields = [
        "body", "title"
    ];

    /**
     * Check if required fields are present.
     * If an error occurs returns an error message, else return an empty string.
     * @param array $data
     * @return string
     */
    private function checkRequiredUserFields(array $data) {

        $keys = array_keys($data);

        // Are there any keys?
        if (count($keys) == 0) return "data_not_readable";

        // appId
        if (array_search($this->requiredUserFields[0], $keys) === false) return "app_id_not_set";

        // userId
        if (array_search($this->requiredUserFields[1], $keys) === false) return "user_id_not_set";

        // deviceType
        if (array_search($this->requiredUserFields[2], $keys) === false) return "device_token_not_set";

        // deviceType
        if (array_search($this->requiredUserFields[2], $keys) === false) return "device_type_not_set";

        return "";
    }

    /**
     * Check if required fields are present.
     * If an error occurs returns an error message, else return an empty string.
     * @param array $data
     * @return string
     */
    private function checkRequiredNotificationFields(array $data) {

        $keys = array_keys($data);

        // Are there any keys?
        if (count($keys) == 0) return "data_not_readable";

        // body
        if (array_search($this->requiredNotificationFields[0], $keys) === false) return "body_not_set";

        // title
        if (array_search($this->requiredNotificationFields[1], $keys) === false) return "title_not_set";

        return "";
    }

    /**
     * Validate a request.
     * @param Request $request
     * @return array
     */
    private function isValidRequest(Request $request) {

        $headers = $request->headers->all();

        // check if header is set
        if (!array_key_exists("authorization", $headers))
            return [
                false,
                "no_authorization_header"
            ];

        // check if 'Bearer' is contained
        $haystack = strtolower($headers["authorization"][0]);
        if (strpos($haystack, "bearer") === false)
            return [
                false,
                "missing_authorization_keyword"
            ];

        $aToken = explode(" ", $haystack);

        // check if we have a token in hand
        if (count($aToken) !== 2)
            return [
                false,
                "invalid_authorization_header_format"
            ];

        $headerToken = $aToken[1];

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository("AppBundle:AuthToken");

        // check if token exists
        $token = $repo->findOneBy(["token" => $headerToken]);

        if (!$token)
            return [
                false,
                "authorization_token_invalid"
            ];

        // check if token is in valid time range
        $now = new \DateTime();

        if ($now < $token->getValidFrom() || $now > $token->getValidTo())
            return [
                false,
                "authorization_token_inactive"
            ];


        return [true];
    }

    /**
     * Prepare notification and store it in database.
     * @param EntityManager $em
     * @param PushUser $user
     * @param array $apiNotification Notification data received from client
     * @param array $generalPayload General payload, same for all apps
     * @return PushNotification
     */
    private function prepareAndSave(EntityManager $em, PushUser $user, $apiNotification, $generalPayload) {

        // create notification
        $notification = new PushNotification();

        $notification->setBody($apiNotification["message"]);
        $notification->setTitle($apiNotification["title"]);

        if (array_key_exists("icon", $generalPayload))
            $notification->setIcon($generalPayload["icon"]);

        if (array_key_exists("sound", $generalPayload))
            $notification->setIcon($generalPayload["sound"]);

        $notification->setData(json_encode($generalPayload));

        $notification->setCreated(new \DateTime());
        $notification->setModified(new \DateTime());

        $notification->setIsSent(false);

        // assign user <-> notification
        $user->getPushNotifications()->add($notification);
        $notification->setPushUser($user);

        $em->persist($notification);
        $em->persist($user);

        $em->flush();

        return $notification;
    }

    /**
     * Prepare push message from given data, then send it.
     * @param EntityManager $em
     * @param PushNotification $notification
     * @return array
     */
    private function prepareAndSend(EntityManager $em, PushNotification $notification) {

        /* @var PushUser $user */
        $user = $notification->getPushUser();

        $message = null;
        if ($user->getDeviceType() === "ios") {

            $message = new iOSMessage();
            $message->setAPSSound("default");
        }

        if ($user->getDeviceType() === "android") {

            $message = new AndroidMessage();
            $message->setGCM(true);
        }

        $data = [];
        if ($notification->getTitle() !== null) {
            $data["title"] = $notification->getTitle();
        }

        if ($notification->getIcon() !== null) {
            $data["icon"] = $notification->getIcon();
        }

        if ($notification->getSound() !== null) {
            $data["sound"] = $notification->getSound();
        }

        if ($notification->getData() !== null) {
            $data["data"] = $notification->getData();
        }

        $message->setMessage($notification->getBody());
        $message->setData($data);
        $message->setDeviceIdentifier($user->getDeviceToken());

        if ($this->container->get('rms_push_notifications')->send($message)) {

            $notification->setIsSent(true);
            $notification->setModified(new \DateTime());

            $em->persist($notification);

            $em->flush();

            $status = "success";

        } else {

            $status = "failed";
        }

        return [
            "status" => $status,
            "uuid" => $user->getUuid(),
            "deviceToken" => $user->getDeviceToken(),
            "notificationId" => $notification->getId()
        ];
    }

    /**
     * @SWG\Post(
     *     path="/register",
     *     tags={"Registration"},
     *     summary="Register a new device to the push server",
     *     description="Registers a new device to the push server so that it can be served with notifications when necessary.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     operationId="registerAction",
     *     @SWG\Parameter(
     *       name="Authorization",
     *       in="header",
     *       description="Authentication token. Pattern: Bearer [Token]",
     *       required=true,
     *       type="string"
     *     ),
     *     @SWG\Parameter(
     *       name="Data",
     *       in="body",
     *       description="JSON object that is expected by server.",
     *       required=true,
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="appId",
     *             type="string",
     *             description="package identifier"
     *           ),
     *           @SWG\Property(
     *             property="userId",
     *             type="string",
     *             description="unique user identification -> uuid"
     *           ),
     *           @SWG\Property(
     *             property="deviceToken",
     *             type="string",
     *             description="token to identify device"
     *           ),
     *           @SWG\Property(
     *             property="deviceType",
     *             type="string",
     *             description="device type, possible: { 'android', 'ios' }"
     *           ),
     *           @SWG\Property(
     *             property="deviceModel",
     *             type="string",
     *             description="device model"
     *           ),
     *           @SWG\Property(
     *             property="deviceVersion",
     *             type="string",
     *             description="OS version"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=200,
     *       description="OK.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="data",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=403,
     *       description="Invalid authentication.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="error",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=404,
     *       description="Necessary resources could not be found.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="error",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=500,
     *       description="Internal server error.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="error",
     *             type="string"
     *           )
     *         }
     *       )
     *     )
     * )
     *
     * Register new user to receive push notifications.
     * Expects JSON string as representation of:
     * {
     *   "appId":"com.domain.package",                      // identifier package
     *   "userId": "dc4056dae620cc1a3...",                  // unique user identification, e.g. session
     *   "deviceToken": "eIQ7haLKwQk:APA91bF9df9pjBp...",   // token to identify device
     *   "deviceType": "android"                            // device type, possible: { "android", "ios" }
     *   "deviceModel": "hammerhead"                        // device model
     *   "deviceVersion": "6.0.1"                           // OS version
     * }
     * @Route("/register", name="register")
     * @Method({ "POST", "GET" })
     */
    public function registerAction(Request $request) {

        $aIsValid = $this->isValidRequest($request);

        if (!$aIsValid[0]) {
            return new JsonResponse([
                "status" => "failed",
                "error" => $aIsValid[1]
            ], 403);
        }

        $data = $request->getContent();

        if (!empty($data)) {
            $data = json_decode($data, true);
        }

        if (is_array($data) === false) {
            return new JsonResponse([
                "status" => "failed",
                "error" => "no_data_received"
            ], 404);
        }

        $errorMsg = $this->checkRequiredUserFields($data);

        if (!empty($errorMsg)) {
            return new JsonResponse([
                "status" => "failed",
                "error" => $errorMsg
            ], 404);
        }

        // check if device is already registered
        $em = $this->getDoctrine()->getManager();

        $repo = $em->getRepository("AppBundle:PushUser");

        $user = $repo->findOneBy(["deviceToken" => $data["deviceToken"]]);

        if ($user) {

            $user->setStatus("active");

            $em->persist($user);

            $em->flush();

            return new JsonResponse([
                "status" => "device_already_registered",
                "data" => $data
            ], 200);
        }

        try {

            $user = new PushUser();

            $user->setAppId($data["appId"]);
            $user->setUuid($data["uuid"]);
            $user->setDeviceToken($data["deviceToken"]);
            $user->setDeviceType($data["deviceType"]);

            if (array_key_exists("deviceModel", $data))
                $user->setDeviceModel($data["deviceModel"]);

            if (array_key_exists("deviceVersion", $data))
                $user->setDeviceVersion($data["deviceVersion"]);

            $user->setCreated(new \DateTime());
            $user->setModified(new \DateTime());
            $user->setStatus("active");

            $em = $this->getDoctrine()->getManager();

            $em->persist($user);

            $em->flush();

        } catch (Exception $e) {

            return new JsonResponse([
                "status" => "failed",
                "error" => $e->getMessage()
            ], 500);
        }

        return new JsonResponse([
            "status" => "register_successful",
            "data" => $data
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/unregister",
     *     tags={"Registration"},
     *     summary="Unregister a device from the push server",
     *     description="Unregisters a device from the push server so that it will no longer receive push notifications.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     operationId="unregisterAction",
     *     @SWG\Parameter(
     *       name="Authorization",
     *       in="header",
     *       description="Authentication token. Pattern: Bearer [Token]",
     *       required=true,
     *       type="string"
     *     ),
     *     @SWG\Parameter(
     *       name="Data",
     *       in="body",
     *       description="JSON object that is expected by server.",
     *       required=true,
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="deviceToken",
     *             type="string",
     *             description="Token which identifies the device at notification services from Apple and Google."
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=200,
     *       description="OK.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=403,
     *       description="Invalid authentication.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="error",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=404,
     *       description="Necessary resources could not be found.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="error",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=500,
     *       description="Internal server error.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="error",
     *             type="string"
     *           )
     *         }
     *       )
     *     )
     * )
     *
     * Unregister user to receive push notifications.
     * Expects JSON string as representation of:
     * {
     *    "deviceToken": "eIQ7haLKwQk:APA91bF9df9pjBpYe...."
     * }
     * @Route("/unregister", name="unregister")
     * @Method({ "POST" })
     */
    public function unregisterAction(Request $request) {

        $aIsValid = $this->isValidRequest($request);

        if (!$aIsValid[0]) {
            return new JsonResponse([
                "status" => "failed",
                "error" => $aIsValid[1]
            ], 403);
        }

        $data = $request->getContent();

        if (!empty($data)) {
            $data = json_decode($data, true);
        }

        $token = $data["deviceToken"];

        $em = $this->getDoctrine()->getManager();

        $repo = $em->getRepository("AppBundle:PushUser");

        try {

            $user = $repo->findOneBy(["deviceToken" => $token]);

            if (!$user) {
                return new JsonResponse([
                    "status" => "failed",
                    "error" => "no_user_found"
                ], 404);
            }

            $user->setStatus('inactive');
            $user->setModified(new \DateTime());

            $em->persist($user);

            $em->flush();

        } catch (Exception $e) {

            return new JsonResponse([
                "status" => "failed",
                "error" => $e->getMessage()
            ], 500);
        }

        return new JsonResponse([
            "status" => "unregister_successful"
        ]);
    }

    /**
     * @SWG\Put(
     *     path="/messages/create",
     *     tags={"Message"},
     *     summary="Create push messages",
     *     description="Generates push notifications from given data.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     operationId="createNotificationAction",
     *     @SWG\Parameter(
     *       name="Authorization",
     *       in="header",
     *       description="Authentication token. Pattern: Bearer [Token]",
     *       required=true,
     *       type="string"
     *     ),
     *     @SWG\Parameter(
     *       name="Data",
     *       in="body",
     *       description="JSON object that is expected by server.",
     *       required=true,
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="tokens",
     *             description="Device tokens that need to be notified.",
     *             type="array",
     *             @SWG\Items(type="string")
     *           ),
     *           @SWG\Property(
     *             property="profile",
     *             type="string",
     *             description="Could be 'production' or 'development'"
     *           ),
     *           @SWG\Property(
     *             property="notification",
     *             type="object",
     *             properties={
     *               @SWG\Property(
     *                 property="title",
     *                 type="string"
     *               ),
     *               @SWG\Property(
     *                 property="message",
     *                 type="string"
     *               ),
     *               @SWG\Property(
     *                 property="payload",
     *                 type="object",
     *                 properties={
     *                   @SWG\Property(
     *                     property="data",
     *                     type="object",
     *                     description="'data' is a placeholder for any data needs to be passed to the app."
     *                   )
     *                 }
     *               )
     *             }
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=200,
     *       description="OK.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="results",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=403,
     *       description="Invalid authentication.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="error",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=404,
     *       description="Necessary resources could not be found.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="error",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=500,
     *       description="Internal server error.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="error",
     *             type="string"
     *           )
     *         }
     *       )
     *     )
     * )
     *
     * Create a new push notification.
     * {
     *   tokens: [],
     *   profile: 'production' || 'development',
     *   notification: {
     *      title: 'Some Title',
     *      message: $this->message,
     *      payload: $this->payload
     *   }
     * }
     * @Route("messages/create", name="messages_create")
     * @Method({ "PUT" })
     */
    public function createNotificationAction(Request $request) {

        $aIsValid = $this->isValidRequest($request);

        if (!$aIsValid[0]) {
            return new JsonResponse([
                "status" => "failed",
                "error" => $aIsValid[1]
            ], 403);
        }

        // get request data
        $data = $request->getContent();

        if (!empty($data)) {
            $data = json_decode($data, true);
        }

        if (is_array($data) === false) {
            return new JsonResponse([
                "status" => "failed",
                "error" => "no_data_received"
            ], 404);
        }

        // check tokens
        $tokens = $data["tokens"];

        if (count($tokens) <= 0) {
            return new JsonResponse([
                "status" => "failed",
                "error" => "no_tokens_found"
            ], 404);
        }

        // check if notification is (at least) present
        if (!array_key_exists("notification", $data)) {
            return new JsonResponse([
                "status" => "failed",
                "error" => "no_notification_found"
            ], 404);
        }

        $results = [];

        foreach ($tokens as $deviceToken) {

            $apiNotification = $data["notification"];

            $generalPayload = $apiNotification["payload"];

            // get user
            $em = $this->getDoctrine()->getManager();

            $userRepo = $em->getRepository("AppBundle:PushUser");

            $user = $userRepo->findOneBy(["deviceToken" => $deviceToken]);
            if (!$user) {
                $results[] = [
                    "status" => "failed",
                    "error" => "no_user_found",
                    "token" => $deviceToken
                ];
                continue;
            }

            // save to db
            $notification = $this->prepareAndSave($em, $user, $apiNotification, $generalPayload);

            // send message
            $results[] = $this->prepareAndSend($em, $notification);
        }

        return new JsonResponse([
            "status" => "messages_created",
            "results" => $results
        ]);
    }

    /**
     * @SWG\Post(
     *     path="/messages/send",
     *     tags={"Message"},
     *     summary="Send push messages",
     *     description="Prepares and sends outstanding push notifications.",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     operationId="pushAction",
     *     @SWG\Parameter(
     *       name="Authorization",
     *       in="header",
     *       description="Authentication token. Pattern: Bearer [Token]",
     *       required=true,
     *       type="string"
     *     ),
     *     @SWG\Response(
     *       response=200,
     *       description="OK.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="results",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=403,
     *       description="Invalid authentication.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *           @SWG\Property(
     *             property="status",
     *             type="string"
     *           ),
     *           @SWG\Property(
     *             property="error",
     *             type="string"
     *           )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=404,
     *       description="Necessary resources could not be found.",
     *       @SWG\Schema(
     *          type="object",
     *          properties={
     *            @SWG\Property(
     *              property="status",
     *              type="string"
     *            ),
     *            @SWG\Property(
     *              property="error",
     *              type="string"
     *            )
     *         }
     *       )
     *     ),
     *     @SWG\Response(
     *       response=500,
     *       description="Internal server error.",
     *       @SWG\Schema(
     *         type="object",
     *         properties={
     *            @SWG\Property(
     *              property="status",
     *              type="string"
     *            ),
     *            @SWG\Property(
     *              property="error",
     *              type="string"
     *            )
     *          }
     *       )
     *     )
     * )
     * Send push messages.
     * @Route("messages/send", name="messages_send")
     * @Method({ "POST" })
     */
    public function pushAction(Request $request) {

        $aIsValid = $this->isValidRequest($request);

        if (!$aIsValid[0]) {
            return new JsonResponse([
                "status" => "failed",
                "error" => $aIsValid[1]
            ], 403);
        }

        $em = $this->getDoctrine()->getManager();

        $repo = $em->getRepository("AppBundle:PushNotification");

        $notifications = $repo->findBy(["isSent" => false], ["created" => "ASC"]);

        $results = [];
        try {

            foreach ($notifications as $notification) {

                $results[] = $this->prepareAndSend($em, $notification);
            }

        } catch (Exception $e) {

            return new JsonResponse([
                "status" => "failed",
                "error" => $e->getMessage()
            ], 500);
        }

        return new JsonResponse([
            "status" => (count($results) == 0) ? "no_push_notifications_to_send" : "push_notifications_sent",
            "results" => $results
        ]);
    }
}