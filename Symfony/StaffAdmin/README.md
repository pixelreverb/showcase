staff_admin
===========

A staff administration project.

- Basically, CRUD methods for users
- Registration
- Login
- Translations
- Each user can make an appointment, invite other users to it
- Dashboard shows upcoming appointments (own, invitations)
- Calendar shows appointments (own, invitations)
- CRUD methods for appointments
- iCal support