<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Extension;

use Twig_Extension;

/**
 * Class SafeJsonEncodeExtension
 * Inspired by: https://vivait.co.uk/labs/safely-escape-twigs-json_encode-without-using-raw
 *
 * @package AppBundle\Extension
 */
class SafeJsonEncodeExtension extends Twig_Extension
{
    public function getFilters()
    {
        [
            new \Twig_SimpleFilter(
                'json_encode_filtered',
                [$this, 'applyOurFilter'],
                ['is_safe' => ['html']]
            )
        ];
    }

    public function applyOurFilter($input)
    {
        $array = $this->recursiveSanitizeArray($input);

        return json_encode($array);
    }

    public static function sanitize($input)
    {
        $sanitized = $input;

        if ( ! is_int($sanitized)) {
            $sanitized = filter_var($sanitized, FILTER_SANITIZE_SPECIAL_CHARS);
        } else {
            $newValue = filter_var($sanitized, FILTER_SANITIZE_SPECIAL_CHARS);

            if (is_numeric($newValue))
            {
                $sanitized = intval($newValue);
            } else {
                $sanitized = $newValue;
            }
        }

        return $sanitized;
    }

    public function recursiveSanitizeArray($array)
    {
        $finalArray = [];

        foreach ($array as $key => $value)
        {
            $newKey = self::sanitize($key);

            if (is_array($value))
            {
                $finalArray[$newKey] = $this->recursiveSanitizeArray($value);
            }
            else
            {
                $finalArray[$newKey] = self::sanitize($value);
            }
        }

        return $finalArray;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'safe json encode extension';
    }
}