<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UserController extends BaseController
{
    /**
     * @Route("/query/users/all", name="query_users_all")
     */
    public function queryAllUsersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository("AppBundle:User");

        $result = [];
        foreach ($repo->findAll() as $user)
        {
            // we do not need ourselves within the list
            if ($user->getId() == $this->getActiveUser()->getId())
                continue;

            // also we do not need inactive users
            if ($user->getIsActive() == false)
                continue;

            $obj = [
                "id" => $user->getId(),
                "text" => $user->getUsername()
            ];

            array_push($result, $obj);
        }

        return new JsonResponse($result);
    }
}