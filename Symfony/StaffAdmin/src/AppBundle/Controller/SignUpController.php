<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Type\RegistrationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class SignUpController extends Controller
{
	/**
     * @Route("/signup", name="signup")
     * @Template("security/signup.html.twig")
     */
    public function signupAction(Request $request)
    {
    	$user = new User();
		$form = $this->createForm(RegistrationType::class, $user);

		// happens only on POST
		$form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            // Encode the password (you could also do this via Doctrine listener)
            $password = $this->get('sha256salted_encoder')
                ->encodePassword($user->getPassword(), $user->getSalt());

            $user->setPassword($password);
            $user->setLastLogin(new \DateTime());

            // Save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));

            return $this->redirectToRoute('list_staff');
        }

		return [ 'form' => $form->createView() ];
    }
}