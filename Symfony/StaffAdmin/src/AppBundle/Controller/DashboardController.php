<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DashboardController extends BaseController
{
    /**
     * @Route("/dashboard", name="dashboard")
     * @Template("content/dashboard.html.twig")
     */
    public function dashboardAction()
    {
        $user = $this->getActiveUser();

        $em = $this->getDoctrine()->getManager();

        // get repos
        $userRepo = $em->getRepository("AppBundle:User");
        $appointmentRepo = $em->getRepository("AppBundle:Appointment");

        // count co-workers in total
        $totalUser = $userRepo->countUsers();

        // get coworker distribution among known departments
        $distributionUser = $userRepo->loadUserDepartmentDistribution();

        // count personal appointments in month
        $totalAppointmentsMonth = $appointmentRepo->countAppointmentsInCurrentMonthForUser($user);

        // count personal appointments that have already past
        $totalAppointmentsPassed = $appointmentRepo->countAppointmentsPassedInCurrentMonthForUser($user);

        // count personal appointments today
        $totalAppointmentsToday = $appointmentRepo->countAppointmentsToday($user);

        return [
            'currentUser' => $user,
            'languages' => $this->getLocales(),
            'totalCoWorkers' => $totalUser,
            'totalAppointmentsMonth' => $totalAppointmentsMonth,
            'totalAppointmentsToday' => $totalAppointmentsToday,
            'totalAppointmentsPassed' => $totalAppointmentsPassed,
            'distributionCoWorkers' => $distributionUser
        ];
    }
}