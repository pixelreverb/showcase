<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Department;
use AppBundle\Entity\PhoneNumber;
use AppBundle\Entity\User;
use AppBundle\Form\Type\BasicInfoType;
use AppBundle\Form\Type\DepartmentType;
use AppBundle\Form\Type\PhoneType;
use AppBundle\Form\Type\RegistrationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\BaseController;

class ProfileController extends BaseController
{
    /**
     * @Route("/profile/show/{userId}", name="profile_show")
     * @Template("user/profile.detail.html.twig")
     */
    public function showAction(Request $request, $userId)
    {
        $em = $this->getDoctrine()->getManager();
        $repoUser = $em->getRepository('AppBundle:User');

        $user = $repoUser->findOneBy(array(
            'id' => $userId
        ));

        // basic user data
        $basicForm = $this->createForm(BasicInfoType::class, $user);
        $basicForm->handleRequest($request);
        if($basicForm->isSubmitted())
        {
            $password = $this->get('sha256salted_encoder')
                ->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();
        }

        // phone number
        $phoneForm = $this->createForm(PhoneType::class, $user);
        $phoneForm->handleRequest($request);
        if($phoneForm->isSubmitted())
        {
            $em->persist($user);
            $em->flush();
        }

        // assigned department
        $department = new Department();
        $departmentForm = $this->createForm(DepartmentType::class, $department);
        $departmentForm->handleRequest($request);
        if($departmentForm->isSubmitted())
        {
            $departmentSelected = $departmentForm->getData()->getDepartment();
            $user->setDepartment($departmentSelected);

            $em->persist($user);
            $em->flush();
        }

        return [
            'currentUser' => $this->getActiveUser(),
            "languages" => $this->getLocales(),
            'user' => $user,
            'formBasic' => $basicForm->createView(),
            'formPhone' => $phoneForm->createView(),
            'formDepartment' => $departmentForm->createView()
        ];
    }

    /**
     * @Route("/profile/new", name="profile_new")
     * @Template("user/profile.create.html.twig")
     */
    public function newAction(Request $request) {

        $user = new User();

        $basicForm = $this->createForm(BasicInfoType::class, $user);
        $phoneForm = $this->createForm(PhoneType::class, $user);

        $department = new Department();
        $departmentForm = $this->createForm(DepartmentType::class, $department);

        $basicForm->handleRequest($request);
        $phoneForm->handleRequest($request);
        $departmentForm->handleRequest($request);

        if ($basicForm->isSubmitted() && $basicForm->isValid() &&
            $phoneForm->isSubmitted() && $phoneForm->isValid() &&
            $departmentForm->isSubmitted() && $departmentForm->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            // encode password
            $password = $this->get('sha256salted_encoder')
                ->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);

            // cannot be null, so we have to set it
            $user->setLastLogin(new \DateTime("1970-01-01T00:00:00Z"));

            // set department
            $departmentSelected = $departmentForm->getData()->getDepartment();
            $user->setDepartment($departmentSelected);

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute("list_staff");
        }

        return [
            'currentUser' => $this->getActiveUser(),
            "languages" => $this->getLocales(),
            'formBasic' => $basicForm->createView(),
            'formPhone' => $phoneForm->createView(),
            'formDepartment' => $departmentForm->createView()
        ];
    }
}