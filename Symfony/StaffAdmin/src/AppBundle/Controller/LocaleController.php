<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class LocaleController extends BaseController
{
    /**
     * @Route("/locale/{locale}", name="change_locale")
     */
    public function changeLocaleAction(Request $request, $locale)
    {
        if (!$this->get('session')->isStarted())
            $this->get('session')->start();

        $this->get('session')->set('_locale', $locale);

        return $this->redirect($request->headers->get('referer'));
    }
}