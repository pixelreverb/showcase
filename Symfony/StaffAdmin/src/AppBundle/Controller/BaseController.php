<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    protected function getActiveUser()
    {
        return $this->get('security.token_storage')->getToken()->getUser();
    }

    protected function getLocales()
    {
        return [

            [
                "language" => "english",
                "code" => "en"
            ],
            [
                "language" => "deutsch",
                "code" => "de"
            ]

        ];
    }
}