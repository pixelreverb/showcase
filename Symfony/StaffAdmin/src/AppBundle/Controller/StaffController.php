<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Address;
use AppBundle\Entity\PhoneNumber;
use AppBundle\Form\Type\AddressType;
use AppBundle\Form\Type\PhoneType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\BaseController;

class StaffController extends BaseController
{
    /**
     * @Route("/list/staff", name="list_staff")
     * @Template("content/list.staff.html.twig")
     */
    public function listStaffAction(Request $request)
    {
        // as this is the route specified after login
        // we logout those who are marked as inactive
        // TODO: would be better to have this validation during login process
        if (!$this->getActiveUser()->getIsActive())
            return $this->redirectToRoute('logout');

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:User');

        $users = $repo->findBy(array(), array('surname' => 'ASC', 'forename' => 'ASC'));

        return [
            'currentUser' => $this->getActiveUser(),
            'languages' => $this->getLocales(),
            'users' => $users
        ];
    }


}
