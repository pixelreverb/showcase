<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class AddressController extends BaseController
{
    /**
     * @Route("/query/addresses", name="query_addresses_all")
     */
    public function queryAddressesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository("AppBundle:Address");

        $result = [];
        foreach ($repo->findAll() as $address)
        {
            $obj = [
                "id" => $address->getId(),
                "text" => $address->__toString()
            ];

            array_push($result, $obj);
        }

        return new JsonResponse($result);
    }
}