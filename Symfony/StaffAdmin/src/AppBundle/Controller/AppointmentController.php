<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Address;
use AppBundle\Entity\Appointment;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AddressType;
use AppBundle\Form\Type\AppointmentType;
use AppBundle\Form\Type\ICalType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Event;
use Eluceo\iCal\Property\Event\Attendees;
use Eluceo\iCal\Property\Event\Organizer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AppointmentController extends BaseController
{
    /**
     * List all appointments for a certain user.
     * By default the user appointments are displayed for is the currently active user,
     * but this can be changed via the select form field.
     *
     * The template will display appointments in a calendar and in form of a list.
     *
     * @Route("/list/appointments", name="list_appointments")
     * @Template("content/list.appointments.html.twig")
     */
    public function appointmentAction(Request $request)
    {
        $user = new User();
        $form = $this->createUserSelectForm($user);

        $form->handleRequest($request);
        if(!$form->isSubmitted())
        {
            $user = $this->getActiveUser();
        }
        else
        {
            $user->setId($request->request->get('form')["username"]);
        }

        $em = $this->getDoctrine()->getManager();
        $appointmentRepo = $em->getRepository('AppBundle:Appointment');
        $appointments = $appointmentRepo->loadAppointmentsForUser($user);

        $days = $this->buildDaysForCalendar($appointments);

        return [
            'currentUser' => $this->getActiveUser(),
            "languages" => $this->getLocales(),
            'appointments' => $appointments,
            'days' => $days,
            'form' => $form->createView()
        ];
    }

    /**
     * @param \AppBundle\Entity\User $user
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createUserSelectForm(User $user)
    {
        return $this->createFormBuilder($user)
                    ->add('username', EntityType::class, [
                        "label" => "User",
                        "class" => 'AppBundle\Entity\User',
                        "placeholder" => '_which_calendar',
                        "multiple"=>false,
                        "query_builder" => function(EntityRepository $repository)
                        {
                            $qb = $repository->createQueryBuilder('u');
                            return $qb->where("u.isActive = true"); // do not show inactive users
                        },
                        "data" => $this->getActiveUser()
                    ])
                    ->getForm();
    }

    /**
     * @param $appointments
     * @return array
     */
    private function buildDaysForCalendar($appointments)
    {
        $days = [];
        foreach ($appointments  as $appointment)
        {
            $startDate = $appointment->getStartDate();
            $endDate = $appointment->getEndDate();

            // get difference in number of days
            $diff = date_diff($endDate, $startDate)->format('%a');

            $day = $startDate->format('d');
            for ($i = 0; $i <= $diff; $i++)
            {
                if (!isset($days[($day + $i)]))
                    $days[($day + $i)] = [];

                array_push($days[($day + $i)], $appointment);
            }
        }
        return $days;
    }

    /**
     * Create a new appointment.
     *
     * @Route("/appointment/new", name="create_appointment")
     * @Template("appointment/appointment.create.html.twig")
     */
    public function newAction(Request $request)
    {
        $appointment = new Appointment();
        $appointmentForm = $this->createForm(AppointmentType::class, $appointment);

        $address = new Address();
        $addressForm = $this->createForm(AddressType::class, $address);

        $appointmentForm->handleRequest($request);
        $addressForm->handleRequest($request);
        if ($appointmentForm->isSubmitted() && $addressForm->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();

            // owner
            $this->persistAppointmentAndOwner($em, $appointment, $request);

            // other participants
            $this->persistAppointmentAndParticipants($em, $appointment, $appointmentForm);

            // set address
            $this->persistAppointmentAndAddress($em, $appointment, $address, $request);

            return $this->redirectToRoute('list_appointments');
        }

        return [
            'currentUser' => $this->getActiveUser(),
            "languages" => $this->getLocales(),
            'appointmentForm' => $appointmentForm->createView(),
            'addressForm' => $addressForm->createView()
        ];
    }

    /**
     * Build choices for location select field.
     * Will contain all know locations.
     */
    private function buildChoices()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository("AppBundle:Address");

        return $repo->findAll();
    }

    /**
     * Persist appointment and owner.
     *
     * @param EntityManager $em
     * @param Appointment $appointment
     * @param Request $request
     */
    private function persistAppointmentAndOwner(EntityManager $em, Appointment $appointment, Request $request)
    {
        // convert date
        $start = $request->request->get('appointment')["startDate"];
        $stop = $request->request->get('appointment')["endDate"];

        $appointment->setStartDate(new \DateTime($start));
        $appointment->setEndDate(new \DateTime($stop));

        // current user
        $user = $this->getActiveUser();
        $user->addAppointment($appointment);
        $appointment->addUser($user);
        $appointment->setOwner($user->getId());

        $em->persist($user);
        $em->persist($appointment);
        $em->flush();
    }

    /**
     * Persist appointment and participants.
     *
     * @param EntityManager $em
     * @param Appointment $appointment
     * @param $appointmentForm
     */
    private function persistAppointmentAndParticipants(EntityManager $em, Appointment $appointment, $appointmentForm)
    {
        $formData = $appointmentForm->getData();
        $users = $formData->getUsers();

        foreach ($users as $u)
        {
            if(!$appointment->hasUser($u))
                $appointment->addUser($u);

            if(!$u->hasAppointment($appointment))
                $u->addAppointment($appointment);

            $em->persist($u);
            $em->persist($appointment);
            $em->flush();
        }
    }

    /**
     * Persist appointment and address.
     *
     * @param EntityManager $em
     * @param Appointment $appointment
     * @param Address $address
     * @param $request
     */
    private function persistAppointmentAndAddress(EntityManager $em, Appointment $appointment, Address $address, $request)
    {
        // check select field
        // if it is set a value was chosen
        if (array_key_exists("addresses", $request->request->get('address')))
        {
            $selectedAddressId = $request->request->get('address')["addresses"];

            $repo = $em->getRepository("AppBundle:Address");
            $address =  $repo->findOneBy([
               "id" => $selectedAddressId
            ]);
        }

        $address->addAppointment($appointment);
        $appointment->setAddress($address);

        $em->persist($address);
        $em->persist($appointment);
        $em->flush();
    }

    /**
     * Show appointment details and allow alternation of the data.
     *
     * @Route("/appointment/show/{appointmentId}", name="appointment_show")
     * @Template("appointment/appointment.detail.html.twig")
     * @param Request $request
     * @param $appointmentId
     * @return array
     */
    public function showAction(Request $request, $appointmentId)
    {
        $em = $this->getDoctrine()->getManager();

        $appointmentRepo = $em->getRepository("AppBundle:Appointment");
        $appointment = $appointmentRepo->findOneBy([
            "id" => $appointmentId
        ]);

        $form = $this->createForm(AppointmentType::class, $appointment);

        $address = $appointment->getAddress();
        $addressForm = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            // convert date
            $start = $request->request->get('appointment')["startDate"];
            $stop = $request->request->get('appointment')["endDate"];

            $appointment->setStartDate(new \DateTime($start));
            $appointment->setEndDate(new \DateTime($stop));

            // persist address if there is a new one
            $this->persistAppointmentAndAddress($em, $appointment, $address, $request);

            $em->persist($appointment);
            $em->flush();
        }

        $iCalForm = $this->createForm(ICalType::class);

        return [
            "currentUser" => $this->getActiveUser(),
            "languages" => $this->getLocales(),
            "appointment" => $appointment,
            "form" => $form->createView(),
            "addressForm" => $addressForm->createView(),
            "iCalForm" => $iCalForm->createView()
        ];
    }

    /**
     * Remove an appointment.
     * It will be cleared from the database completely.
     *
     * @Route("/appointment/remove/{appointmentId}", name="appointment_remove")
     * @param Request $request
     * @param $appointmentId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Request $request, $appointmentId)
    {
        $em = $this->getDoctrine()->getManager();

        $appointmentRepo = $em->getRepository("AppBundle:Appointment");
        $appointment = $appointmentRepo->findOneBy([
            "id" => $appointmentId
        ]);

        $em->remove($appointment);
        $em->flush();

        return $this->redirectToRoute('list_appointments');
    }

    /**
     * @param EntityManager $em
     * @param Event $vEvent
     * @param Appointment $appointment
     */
    private function setEventOrganizer(EntityManager $em, Event $vEvent, Appointment $appointment)
    {
        // get appointment owner
        $userRepo = $em->getRepository("AppBundle:User");
        $user = $userRepo->findOneBy([
            "id" => $appointment->getOwner()
        ]);

        $organizerValue = "MAILTO:".$user->getEmail();
        $organizerParam = $user->getSurname()." ".$user->getForename();
        $organizer = new Organizer($organizerValue, ["CN" => $organizerParam]);
        $vEvent->setOrganizer($organizer);
    }

    /**
     * @param Event $vEvent
     * @param Appointment $appointment
     */
    private function setEventData(Event $vEvent, Appointment $appointment)
    {
        $vEvent->setDtStart($appointment->getStartDate());
        $vEvent->setDtEnd($appointment->getEndDate());
        $vEvent->setNoTime(false);
        $vEvent->setUseUtc(false);
        $vEvent->setSummary($appointment->getTitle());
        $vEvent->setDescription($appointment->getDescription());
    }

    /**
     * @param Event $vEvent
     * @param Appointment $appointment
     */
    private function setEventParticipants(Event $vEvent, Appointment $appointment)
    {
        $participants = $appointment->getUsers();
        if (!isset($participants)) return;

        $attendees = new Attendees();
        foreach ($participants as $participant)
        {
            $attendeeValue = "MAILTO:".$participant->getEmail();
            $attendeeParam = $participant->getSurname()." ".$participant->getForename();

            $attendees->add($attendeeValue, [ "CN" => $attendeeParam ]);
        }
        $vEvent->setAttendees($attendees);
    }

    /**
     * @param Event $vEvent
     * @param Appointment $appointment
     */
    private function setEventLocation(Event $vEvent, Appointment $appointment)
    {
        $address = $appointment->getAddress();
        if ( isset($address) )
            $vEvent->setLocation($address->__toString());
    }

    /**
     * @Route("/iCal/create/{appointmentId}", name="iCal_create")
     */
    public function createICalAction(Request $request, $appointmentId)
    {
        // get appointment
        $em = $this->getDoctrine()->getManager();

        $appointmentRepo = $em->getRepository("AppBundle:Appointment");
        $appointment = $appointmentRepo->findOneBy([
            "id" => $appointmentId
        ]);

        // create iCal data
        $vCalendar = new Calendar(uniqid());

        $vEvent = new Event();

        $this->setEventOrganizer($em, $vEvent, $appointment);
        $this->setEventData($vEvent, $appointment);
        $this->setEventParticipants($vEvent, $appointment);
        $this->setEventLocation($vEvent, $appointment);

        $vCalendar->addComponent($vEvent);

        $response = new Response();
        $response->headers->set("Content-Type", "text/calendar; charset=utf-8");
        $response->headers->set('Content-Disposition', 'attachment; filename="cal.ics"');
        $response->setContent($vCalendar->render());
        $response->setStatusCode(200);

        return $response;
    }
}