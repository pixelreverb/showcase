<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="app_users")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $forename;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(name="share_calendar", type="boolean")
     */
    private $shareCalendar;

    /**
     * @ORM\Column(name="last_login", type="datetime")
     */
    private $lastLogin;

    /**
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="users")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    private $role;

    /**
     * @ORM\ManyToOne(targetEntity="Department", inversedBy="users")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     */
    private $department;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $baseNumber;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $extensionInternal;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $extensionExternal;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $mobilePhoneNumber;

    /**
     * ORM\OneToOne(targetEntity="PhoneNumber", inversedBy="user")
     * ORM\JoinColumn(name="phone_number_id", referencedColumnName="id")
     */
//    private $phoneNumber;

    /**
     * @ORM\ManyToMany(targetEntity="Appointment", mappedBy="users")
     */
    private $appointments;

    public function __construct()
    {
        $this->isActive = true;
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
    }

    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getSalt()
    {
        return 'daf3ae813ad102e1014c6a5cb9b3a7fb';
    }
    
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function getLastLogin() {
        return $this->lastLogin;
    }

    public function setLastLogin($lastLogin) {
        $this->lastLogin = $lastLogin;
    }

    public function eraseCredentials()
    {
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    /**
     * @return mixed
     */
    public function getShareCalendar()
    {
        return $this->shareCalendar;
    }

    /**
     * @param mixed $shareCalendar
     */
    public function setShareCalendar($shareCalendar)
    {
        $this->shareCalendar = $shareCalendar;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
            $this->isActive,
            $this->baseNumber,
            $this->extensionInternal,
            $this->extensionExternal
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            $this->isActive,
            $this->baseNumber,
            $this->extensionInternal,
            $this->extensionExternal
        ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add role
     *
     * @param \AppBundle\Entity\Role $role
     *
     * @return User
     */
    public function addRole(\AppBundle\Entity\Role $role)
    {
        $this->role[] = $role;

        return $this;
    }

    /**
     * Remove role
     *
     * @param \AppBundle\Entity\Role $role
     */
    public function removeRole(\AppBundle\Entity\Role $role)
    {
        $this->role->removeElement($role);
    }

    /**
     * Get role
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add department
     *
     * @param \AppBundle\Entity\Department $department
     *
     * @return User
     */
    public function addDepartment(\AppBundle\Entity\Department $department)
    {
        $this->department[] = $department;

        return $this;
    }

    /**
     * Remove department
     *
     * @param \AppBundle\Entity\Department $department
     */
    public function removeDepartment(\AppBundle\Entity\Department $department)
    {
        $this->department->removeElement($department);
    }

    /**
     * Get department
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Add phoneNumber
     *
     * @param \AppBundle\Entity\PhoneNumber $phoneNumber
     *
     * @return User
     */
    public function addPhoneNumber(\AppBundle\Entity\PhoneNumber $phoneNumber)
    {
        $this->phoneNumber[] = $phoneNumber;

        return $this;
    }

    /**
     * Remove phoneNumber
     *
     * @param \AppBundle\Entity\PhoneNumber $phoneNumber
     */
    public function removePhoneNumber(\AppBundle\Entity\PhoneNumber $phoneNumber)
    {
        $this->phoneNumber->removeElement($phoneNumber);
    }

    /**
     * Get phoneNumber
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Add appointment
     *
     * @param \AppBundle\Entity\Appointment $appointment
     *
     * @return User
     */
    public function addAppointment(\AppBundle\Entity\Appointment $appointment)
    {
        $this->appointments[] = $appointment;

        return $this;
    }

    /**
     * Remove appointment
     *
     * @param \AppBundle\Entity\Appointment $appointment
     */
    public function removeAppointment(\AppBundle\Entity\Appointment $appointment)
    {
        $this->appointments->removeElement($appointment);
    }

    /**
     * Get appointments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * Set baseNumber
     *
     * @param string $baseNumber
     *
     * @return User
     */
    public function setBaseNumber($baseNumber)
    {
        $this->baseNumber = $baseNumber;

        return $this;
    }

    /**
     * Get baseNumber
     *
     * @return string
     */
    public function getBaseNumber()
    {
        return $this->baseNumber;
    }

    /**
     * Set extensionInternal
     *
     * @param string $extensionInternal
     *
     * @return User
     */
    public function setExtensionInternal($extensionInternal)
    {
        $this->extensionInternal = $extensionInternal;

        return $this;
    }

    /**
     * Get extensionInternal
     *
     * @return string
     */
    public function getExtensionInternal()
    {
        return $this->extensionInternal;
    }

    /**
     * Set extensionExternal
     *
     * @param string $extensionExternal
     *
     * @return User
     */
    public function setExtensionExternal($extensionExternal)
    {
        $this->extensionExternal = $extensionExternal;

        return $this;
    }

    /**
     * Get extensionExternal
     *
     * @return string
     */
    public function getExtensionExternal()
    {
        return $this->extensionExternal;
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     *
     * @return User
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMobilePhoneNumber()
    {
        return $this->mobilePhoneNumber;
    }

    /**
     * @param mixed $mobilePhoneNumber
     */
    public function setMobilePhoneNumber($mobilePhoneNumber)
    {
        $this->mobilePhoneNumber = $mobilePhoneNumber;
    }

    /**
     * @return mixed
     */
    public function getForename()
    {
        return $this->forename;
    }

    /**
     * @param mixed $forename
     */
    public function setForename($forename)
    {
        $this->forename = $forename;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * { @inheritdoc }
     */
    public function __toString()
    {
        return (string) $this->username;
    }

    /**
     * @param AppBundle/Entity/Appointment $appointment
     * @return boolean
     */
    public function hasAppointment($appointment)
    {
        return $this->getAppointments()->contains($appointment);
    }

    /**
     * Set role
     *
     * @param \AppBundle\Entity\Role $role
     *
     * @return User
     */
    public function setRole(\AppBundle\Entity\Role $role = null)
    {
        $this->role = $role;

        return $this;
    }
}
