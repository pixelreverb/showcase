<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Entity;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        $result = $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function countUsers()
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select($qb->expr()->count('u.id'));

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function loadUserDepartmentDistribution()
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select('d.department', $qb->expr()->count('u.id'));
        $qb->innerJoin('u.department', 'd');
        $qb->groupBy('d.department');

        return $qb->getQuery()->getResult();
    }

}