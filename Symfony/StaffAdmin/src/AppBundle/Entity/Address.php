<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="addresses")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AddressRepository")
 */
class Address implements \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Appointment", mappedBy="address")
     */
    private $appointments;

    /**
     * @ORM\Column(type="string", length=100)
    */
    private $company;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $room;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $place;

    public function __construct()
    {
        $this->appointments = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param mixed $room
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }

    /**
     * { @inheritdoc }
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->company,
            $this->street,
            $this->place
            ));
    }

    /**
     * { @inheritdoc }
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->company,
            $this->street,
            $this->place
            ) = unserialize($serialized);
    }

    /**
     * Add appointment
     *
     * @param \AppBundle\Entity\Appointment $appointment
     *
     * @return Address
     */
    public function addAppointment(\AppBundle\Entity\Appointment $appointment)
    {
        $this->appointments[] = $appointment;

        return $this;
    }

    /**
     * Remove appointment
     *
     * @param \AppBundle\Entity\Appointment $appointment
     */
    public function removeAppointment(\AppBundle\Entity\Appointment $appointment)
    {
        $this->appointments->removeElement($appointment);
    }

    /**
     * Get appointments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * { @inheritdoc }
     */
    public function __toString()
    {
        return $this->company.', '.$this->room.', '.$this->street.', '.$this->place;
    }


}
