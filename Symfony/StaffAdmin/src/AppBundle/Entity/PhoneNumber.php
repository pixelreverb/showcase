<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="phone_numbers")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\RoleRepository")
 */
class PhoneNumber implements \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    private $baseNumber;

    /**
     * @ORM\Column(type="string", length=10, unique=true)
     */
    private $extensionInternal;

    /**
     * @ORM\Column(type="string", length=10, unique=true)
     */
    private $extensionExternal;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="phoneNumber")
     */
    private $user;

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBaseNumber()
    {
        return $this->baseNumber;
    }

    /**
     * @param mixed $baseNumber
     */
    public function setBaseNumber($baseNumber)
    {
        $this->baseNumber = $baseNumber;
    }

    /**
     * @return mixed
     */
    public function getExtensionInternal()
    {
        return $this->extensionInternal;
    }

    /**
     * @param mixed $extensionInternal
     */
    public function setExtensionInternal($extensionInternal)
    {
        $this->extensionInternal = $extensionInternal;
    }

    /**
     * @return mixed
     */
    public function getExtensionExternal()
    {
        return $this->extensionExternal;
    }

    /**
     * @param mixed $extensionExternal
     */
    public function setExtensionExternal($extensionExternal)
    {
        $this->extensionExternal = $extensionExternal;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        // TODO: Implement serialize() method.
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }

    /**
     * Set users
     *
     * @param \AppBundle\Entity\User $users
     *
     * @return PhoneNumber
     */
    public function setUsers(\AppBundle\Entity\User $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \AppBundle\Entity\User
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return PhoneNumber
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
