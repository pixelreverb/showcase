<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Service;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\scalar;

class Sha256Salted implements PasswordEncoderInterface
{
    /**
     * { @inheritdoc }
     */
    public function encodePassword($raw, $salt)
    {
        return hash('sha256', $salt . $raw);
    }

    /**
     * { @inheritdoc }
     */
    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $encoded === $this->encodePassword($raw, $salt);
    }
}