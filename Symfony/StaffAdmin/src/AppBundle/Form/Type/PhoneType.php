<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PhoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('baseNumber', TextType::class, [ 'label' => '_phone' ])
                ->add('extensionInternal', TextType::class, [ 'label' => '_ext_internal' ])
                ->add('extensionExternal', TextType::class, [ 'label' => '_ext_external' ])
                ->add('mobilePhoneNumber', TextType::class, [ 'label' => '_mobile_phone' ]);
    }

    public function getName() {
        return 'phoneNumber';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
        ]);
    }
}