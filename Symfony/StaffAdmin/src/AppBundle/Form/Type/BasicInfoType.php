<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Form\Type;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class BasicInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, ['label'=>'_email'])
            ->add('username', TextType::class, ['label'=>'_username'])
            ->add('forename', TextType::class, ['label'=>'_forename'])
            ->add('surname', TextType::class, ['label'=>'_surname'])
            ->add('password', PasswordType::class,['label'=>'_password'])
            ->add('isActive', ChoiceType::class, [
                'label' => '_active',
                'choices' => [
                    '_yes' => true,
                    '_no' => false
                ]
            ])
            ->add('shareCalendar', ChoiceType::class, [
                'label' => '_share_calendar',
                'choices' => [
                    '_yes' => true,
                    '_no' => false
                ]
            ])
            ->add('role', EntityType::class, [
                "label" => "_role",
                "class" => 'AppBundle\Entity\Role',
                "placeholder" => '_role_choose',
                "multiple"=>false
            ]);
    }

    public function getName()
    {
        return 'basic info';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
        ]);
    }

}