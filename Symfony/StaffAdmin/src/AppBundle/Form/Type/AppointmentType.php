<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Form\Type;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AppointmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label'=>'_title'
            ])
            ->add('description', TextareaType::class, [
                'label'=>'_description'
            ])
            ->add('startDate', DateType::class,[
                'label'=>'_start_date',
                'widget' => 'single_text',
                'html5' => false
             ])
            ->add('endDate', DateType::class,[
                'label'=>'_end_date',
                'widget' => 'single_text',
                'html5' => false
            ])
            ->add('users', Select2EntityType::class, [
                "label" => "_participants",
                "class" => 'AppBundle\Entity\User',
                "placeholder" => '_participant_select',
                'primary_key' => 'id',
                'text_property' => 'username',
                "multiple" => true,
                "remote_route" => 'query_users_all',
                "minimum_input_length" => 0
            ])
        ;
    }

    public function getName()
    {
        return 'appointment';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Appointment',
        ]);
    }
}