<?php
/**
 * Author: Christian Hansen
 * E-Mail: hansen.christian@outlook.de
 * Date: 14.09.16
 */

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('company', TextType::class, [
                    'label' => '_company',
                    'required' => false
                ])
                ->add('room', TextType::class, [
                    'label' => '_room',
                    'required' => false
                ])
                ->add('street', TextType::class, [
                    'label' => '_street_no',
                    'required' => false
                ])
                ->add('place', TextType::class, [
                    'label' => '_zip_city',
                    'required' => false
                ])
                ->add('addresses', Select2EntityType::class, [
                    'mapped' => false,
                    "label" => "_location",
                    "class" => 'AppBundle\Entity\Address',
                    "placeholder" => '_address_select',
                    'primary_key' => 'id',
                    "multiple" => false,
                    "remote_route" => 'query_addresses_all',
                    "minimum_input_length" => 0,
                    "allow_clear" => true,
                    'required' => false
                ]);
    }

    public function getName() {
        return 'address';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Address',
        ]);
    }
}